### Pré-requisitos

Antes de começar, você vai precisar ter instalado em sua máquina as seguintes ferramentas:
[Node.js](https://nodejs.org/en/).

### 🎲 Rodando o front end

```bash
# Instale as dependências
$ npm install

# Execute a aplicação em modo de desenvolvimento
$ npm run dev

# O servidor inciará na porta:5173 - acesse <http://localhost:5173/>
```

### Comandos

```bash
# Para corrigir erros de eslint/prettier
$ npm run lint:fix

# Para exibir os erros do eslint/prettier
$ npm run lint
```
