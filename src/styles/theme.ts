const size = {
  mobileS: "320px",
  mobileM: "375px",
  mobileL: "425px",
  tablet: "768px",
  laptop: "1024px",
  laptopL: "1440px",
  desktop: "2560px",
};

export default {
  border: {
    radius: "3rem",
    button: "4px",
  },

  font: {
    family: "'Montserrat', sans-serif;",
    normal: 400,
    bold: 600,
    bolder: 700,
    sizes: {
      xsmall: "1.2rem",
      small: "1.4rem",
      medium: "1.6rem",
      large: "2.8rem",
      xlarge: "4.0rem",
      xxlarge: "5.6rem",
    },
  },

  colors: {
    darkBlue: "#0f2d54",
    primary: "#8d71e2",
    "20-primary": "#c6b8f0",
  },

  spacings: {
    xxsmall: "1rem",
    xsmall: "1.2rem",
    small: "2.4rem",
    medium: "3rem",
    large: "4.0rem",
    xlarge: "4.8rem",
    xxlarge: "5.6rem",
    huge: "7.0rem",
  },

  layers: {
    base: 10,
    menu: 20,
    overlay: 30,
    modal: 40,
    alwaysOnTop: 50,
  },

  device: {
    mobileS: `(min-width: ${size.mobileS})`,
    mobileM: `(min-width: ${size.mobileM})`,
    mobileL: `(min-width: ${size.mobileL})`,
    tablet: `(min-width: ${size.tablet})`,
    laptop: `(min-width: ${size.laptop})`,
    laptopL: `(min-width: ${size.laptopL})`,
    desktop: `(min-width: ${size.desktop})`,
    desktopL: `(min-width: ${size.desktop})`,
  },
} as const;
