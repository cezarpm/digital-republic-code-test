import { createGlobalStyle, css } from "styled-components";

const GlobalStyles = createGlobalStyle`
  ${({ theme }) => css`
    * {
      margin: 0;
      padding: 0;
      box-sizing: border-box;
    }

    body,
    #root {
      min-height: 100vh;
      height: 100%;
    }

    body {
      overflow-x: hidden;
      -webkit-font-smoothing: antialiased;
      -moz-osx-font-smoothing: grayscale;
      font-family:${theme.font.family};} ;
    }
  `}
`;

export default GlobalStyles;
