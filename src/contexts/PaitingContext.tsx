import { createContext, useContext, useEffect, useState } from "react";

const doorHeight = 1.9;
const doorWidth = 0.8;
const doorArea = doorHeight * doorWidth;

const windowHeight = 1.2;
const windowWidth = 2.0;
const windowArea = windowHeight * windowWidth;

export type Wall = {
  height: number;
  width: number;
  windowQuantity: number;
  doorQuantity: number;
};

type PaitingProviderProps = {
  children: React.ReactNode;
};

interface PaitingProviderType {
  addWalls: (wall: Wall) => number;
  walls: Wall[];
  buckets: { item: number; count: number }[];
  paintAmount: number;
  totalArea: number;
  totalBuckets: number;
  clearContext: () => void;
  wallError: string;
}

const initialState = {
  addWalls() {
    throw new Error("Context not yet initialized.");
  },
  setTotalBuckets() {
    throw new Error("Context not yet initialized.");
  },

  clearContext() {
    throw new Error("Context not yet initialized.");
  },
  walls: [],
  buckets: [],
  paintAmount: 0,
  totalArea: 0,
  totalBuckets: 0,
  wallError: "",
};

const StateContext = createContext<PaitingProviderType>(initialState);

export const PaitingProvider = ({ children }: PaitingProviderProps) => {
  const [walls, setWalls] = useState<Wall[]>([]);
  const [buckets, setBuckets] = useState<{ item: number; count: number }[]>([]);
  const [paintAmount, setPaintAmout] = useState(0);
  const [totalArea, setTotalArea] = useState(0);
  const [totalBuckets, setTotalBuckets] = useState(0);
  const [wallError, setWallError] = useState("");

  const addWalls = (wall: Wall) => {
    setWallError("");

    const wallArea = calculateWallArea(wall);

    if (wallArea === 0) {
      return 0;
    }

    setWalls((walls) => [...walls, wall]);

    return wallArea;
  };

  const calculateWallArea = ({
    doorQuantity = 0,
    height,
    width,
    windowQuantity = 0,
  }: Wall) => {
    const wallArea = height * width;
    const doorAreaTotal = doorArea * doorQuantity;
    const windowAreaTotal = windowArea * windowQuantity;

    const doorsAndWindowsArea = doorAreaTotal + windowAreaTotal;

    // Nenhuma parede pode ter menos de 1 metro quadrado nem mais de 50 metros quadrados, mas podem possuir alturas e larguras diferentes
    if (wallArea < 1 || wallArea > 50) {
      setWallError("A área da parede deve ser entre 1 e 50 metros quadrados");
      return 0;
    }

    // A altura de paredes com porta deve ser, no mínimo, 30 centímetros maior que a altura da porta
    if (doorQuantity > 0 && height <= doorHeight + 0.3) {
      setWallError(
        "A altura da parede deve ser, no mínimo, 30 centímetros maior que a altura da porta",
      );
      return 0;
    }

    // O total de área das portas e janelas deve ser no máximo 50% da área de parede
    if (doorsAndWindowsArea > wallArea / 2) {
      setWallError(
        "A área total das portas e janelas deve ser no máximo 50% da área da parede",
      );
      return 0;
    }

    return wallArea;
  };

  const calculatePaintAmountInLitros = (wallArea: number) => {
    return wallArea / 5;
  };

  const calculateBucketAmount = (paintAmount: number) => {
    let actualPaintAmount = paintAmount;

    const bucketsToBuy = [];
    const buckets = [0.5, 2.5, 3.6, 18];

    while (actualPaintAmount > 0) {
      const removedExtraSizedBuckets = buckets.filter(
        (bucket) => bucket < actualPaintAmount + 0.6,
      );

      const bucket = Math.max(...removedExtraSizedBuckets);

      actualPaintAmount = actualPaintAmount - bucket;
      bucketsToBuy.push(bucket);
    }

    return bucketsToBuy;
  };

  const groupSameBuckets = (buckets: number[]): { item: number; count: number }[] => {
    return buckets.reduce((result: { item: number; count: number }[], item) => {
      const found = result.find((i) => i.item === item);

      if (found) {
        found.count++;
      } else {
        result.push({ item, count: 1 });
      }

      return result;
    }, []);
  };

  const clearContext = () => {
    setWalls([]);
    setBuckets([]);
    setPaintAmout(0);
    setTotalArea(0);
  };

  useEffect(() => {
    if (walls.length === 3) {
      const wallsCalculated = walls.map((wall) => calculateWallArea(wall));

      const totalArea = wallsCalculated.reduce((acc, wall) => acc + wall, 0);
      setTotalArea(Math.round(totalArea));

      const paintAmountInLitros = calculatePaintAmountInLitros(totalArea);
      setPaintAmout(paintAmountInLitros);

      const bucketAmount = calculateBucketAmount(paintAmountInLitros);
      setTotalBuckets(bucketAmount.reduce((acc, bucket) => acc + bucket, 0));

      const sameBuckets = groupSameBuckets(bucketAmount);
      setBuckets(sameBuckets);
    }
  }, [walls]);

  return (
    <StateContext.Provider
      value={{
        walls,
        addWalls,
        buckets,
        paintAmount,
        totalArea,
        totalBuckets,
        wallError,
        clearContext,
      }}
    >
      {children}
    </StateContext.Provider>
  );
};

export const usePaitingContext = () => useContext(StateContext);
