import styled, { css } from "styled-components";

export const Wrapper = styled.button<{ disabled: boolean }>`
  ${({ theme, disabled }) => css`
    background-color: ${theme.colors.primary}};
    padding: 8px 16px;
    border-radius: ${theme.border.button};
    font-size: 22px;
    color:white;
    font-weight: bold;
    border: none;
    cursor: pointer;

    ${disabled && `background-color: ${theme.colors["20-primary"]}; `}
  }
  `}
`;
