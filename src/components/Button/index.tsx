import * as S from "./styles";

type ButtonProps = {
  children: React.ReactNode;
  onClick?: () => void;
  disabled?: boolean;
  type?: "button" | "submit" | "reset";
};

const Button = ({
  children,
  onClick,
  disabled = false,
  type = "button",
}: ButtonProps) => {
  return (
    <S.Wrapper type={type} disabled={disabled} onClick={onClick}>
      {children}
    </S.Wrapper>
  );
};

export default Button;
