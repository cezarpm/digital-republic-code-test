import styled, { css } from "styled-components";

export const Wrapper = styled.div`
  margin: 10px 0;
`;

export const Input = styled.input`
  ${({ theme }) => css`
    width: 100%;
    padding: 8px;
    font-size: 16px;
    border: 1px solid #ccc;

    :focus {
      outline: none !important;
      border-color: ${theme.colors.primary};
    }
  `}
`;

export const Label = styled.label`
  font-size: 18px;
  margin-top: 5px;
`;
