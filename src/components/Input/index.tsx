import { UseFormRegister } from "react-hook-form";

import * as S from "./styles";

export type InputProps = {
  label: string;
  placeholder: string;
  type: string;
  name: string;
  register: UseFormRegister<any>;
};

const Input = ({ label, placeholder, type = "text", register, name }: InputProps) => {
  return (
    <S.Wrapper>
      <S.Label>{label}</S.Label>
      <S.Input
        placeholder={placeholder}
        type={type}
        {...register(name, {
          required: true,
          valueAsNumber: type === "number" ? true : false,
        })}
        step="0.01"
      />
    </S.Wrapper>
  );
};

export default Input;
