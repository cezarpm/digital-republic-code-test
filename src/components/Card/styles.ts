import styled from "styled-components";

export const Card = styled.div`
  width: 600px;
  padding: 20px;
  border-radius: 8px;
  background-color: #f5f5f5;
  border: 1px solid #dcdcdc;
`;
