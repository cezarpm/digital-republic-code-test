import Button from "components/Button";
import Card from "components/Card";
import Default from "layout/Default";
import { NavLink } from "react-router-dom";

import * as S from "./styles";

const Home = () => {
  return (
    <Default>
      <Card>
        <S.Title>Seja bem vindo(a) a Calculadora de pinturas! </S.Title>

        <S.Text>Olá! Vamos começar a pintar a sua casa!</S.Text>
        <S.Text>Para isso vou precisar de algumas informações.</S.Text>

        <S.ButtonWrapper>
          <NavLink to={"/paint-journey"}>
            <Button>Começar</Button>
          </NavLink>
        </S.ButtonWrapper>
      </Card>
    </Default>
  );
};

export default Home;
