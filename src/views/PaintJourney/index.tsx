import Default from "layout/Default";
import { useState } from "react";

import BucketsCalculate from "./steps/BucketsCalculate";
import RegisterWall from "./steps/RegisterWall";

const PaintJourney = () => {
  const [journeyStep, setJourneyStep] = useState<number>(0);

  const handleNextStep = () => {
    setJourneyStep(journeyStep + 1);
  };

  const handlePreviousStep = () => {
    setJourneyStep(journeyStep - 1);
  };

  return (
    <Default>
      {journeyStep === 0 && <RegisterWall handleNextStep={handleNextStep} />}

      {journeyStep === 1 && <BucketsCalculate />}
    </Default>
  );
};

export default PaintJourney;
