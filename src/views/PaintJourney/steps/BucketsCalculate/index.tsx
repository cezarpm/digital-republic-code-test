import Button from "components/Button";
import Card from "components/Card";
import { usePaitingContext } from "contexts/PaitingContext";
import { useNavigate } from "react-router-dom";

import * as S from "./styles";

const BucketsCalculate = () => {
  const navigate = useNavigate();

  const { totalArea, paintAmount, buckets, clearContext, totalBuckets } =
    usePaitingContext();

  const handleClick = () => {
    clearContext();
    navigate("/");
  };

  return (
    <div>
      <S.Title>O cálculo está completo!</S.Title>

      <S.Title>
        Aqui está a quantidade de baldes que você precisará para pintar suas paredes.
      </S.Title>

      <S.Flex>
        <Card>
          <S.Title>Informações adicionais!</S.Title>

          <p>
            <S.Span>Área total:</S.Span> {totalArea}m²
          </p>

          <p>
            <S.Span>Quantidade de tinta necessárias:</S.Span> {paintAmount.toFixed(1)}L
          </p>
        </Card>

        <Card>
          <S.Title>
            Lista de baldes:{" "}
            {buckets.reduce((partialSum, a) => (partialSum += a.count), 0)}{" "}
          </S.Title>

          {buckets.map((bucket, index) => (
            <div key={index}>
              <p>
                <S.Span>{bucket.count}x</S.Span> balde de {bucket.item}L
              </p>
            </div>
          ))}

          <p>
            <S.Span>Soma de litro dos baldes:</S.Span> {totalBuckets}L
          </p>
        </Card>
      </S.Flex>

      <S.ButtonWrapper>
        <Button onClick={handleClick}>Concluído</Button>
      </S.ButtonWrapper>
    </div>
  );
};

export default BucketsCalculate;
