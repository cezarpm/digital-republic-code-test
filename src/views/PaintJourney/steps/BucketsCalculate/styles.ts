import styled, { css } from "styled-components";

export const ButtonWrapper = styled.div`
  display: flex;
  flex-direction: column;
  gap: 10px;
  justify-content: center;
  align-items: center;
  margin-top: 20px;
`;

export const Title = styled.h1`
  font-size: 18px;
  text-align: center;
  font-weight: 500;
  margin: 5px 0;
`;

export const Span = styled.span`
  ${({ theme }) => css`
    font-size: 15px;
    font-weight: bold;
    color: ${theme.colors.primary};
  `}
`;

export const Flex = styled.div`
  display: flex;
  gap: 10px;
  justify-content: center;
  min-height: 130px;
  text-align: center;
`;
