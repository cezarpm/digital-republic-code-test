import * as yup from "yup";

export const registerWallSchema = yup.object().shape({
  height: yup.number().required("Must enter height"),
  width: yup.number().required("Must enter width"),

  showWindow: yup.boolean(),

  windowQuantity: yup.number().when("showWindow", {
    is: (showWindow: boolean) => showWindow === true,
    then: yup.number().min(1).required(),
  }),

  showDoor: yup.boolean(),

  doorQuantity: yup.number().when("showDoor", {
    is: true,
    then: yup.number().min(1).required("Must enter email address"),
  }),
});
