import { yupResolver } from "@hookform/resolvers/yup";
import Button from "components/Button";
import Card from "components/Card";
import Input from "components/Input";
import { usePaitingContext, Wall } from "contexts/PaitingContext";
import { useForm } from "react-hook-form";

import { registerWallSchema } from "./registerWallSchema";
import * as S from "./styles";

type FormValues = {
  showDoor: boolean;
  showWindow: boolean;
} & Wall;

type RegisterWallProps = {
  handleNextStep: () => void;
};

const wallsDictonary: { [index: number]: string } = {
  0: "primeira parede",
  1: "segunda parede",
  2: "terceira parede",
  3: "quarta parede",
};

const RegisterWall = ({ handleNextStep }: RegisterWallProps) => {
  const { addWalls, walls, wallError } = usePaitingContext();

  const {
    register,
    handleSubmit,
    formState: { isValid },
    watch,
    reset,
    resetField,
  } = useForm<FormValues>({
    resolver: yupResolver(registerWallSchema),
  });

  const triggerCheckboxValidations = (
    checkboxName: "windowQuantity" | "doorQuantity",
  ) => {
    resetField(checkboxName);
  };

  const actualWall = wallsDictonary[walls.length];

  const onSubmitHandler = (wall: Wall) => {
    const wallArea = addWalls(wall);

    if (wallArea === 0) return;

    reset({ showDoor: false, showWindow: false });
  };

  const windowCheckbox = watch("showWindow");
  const doorCheckbox = watch("showDoor");

  return (
    <Card>
      <S.Title>Cadastrar {actualWall}</S.Title>

      {wallError && <S.ErrorMessage> {wallError}</S.ErrorMessage>}

      <S.Form onSubmit={handleSubmit(onSubmitHandler)}>
        <Input
          label={`Largura da ${actualWall}`}
          placeholder={`Digite a largura da ${actualWall}`}
          type="number"
          name="width"
          register={register}
        />

        <Input
          label={`Altura da ${actualWall}`}
          placeholder={`Digite a altura da ${actualWall}`}
          type="number"
          name="height"
          register={register}
        />

        <div>
          <label htmlFor="showWindow">Possui janelas?</label>

          <input
            style={{ margin: "5px" }}
            id="showWindow"
            onClick={() => triggerCheckboxValidations("windowQuantity")}
            {...register("showWindow")}
            type="checkbox"
          />

          {windowCheckbox && (
            <Input
              register={register}
              name="windowQuantity"
              label="Quantas janelas?"
              placeholder=""
              type="number"
            />
          )}
        </div>

        <div>
          <label htmlFor="showDoor">Possui portas?</label>

          <input
            style={{ margin: "5px" }}
            id="showDoor"
            onClick={() => triggerCheckboxValidations("doorQuantity")}
            {...register("showDoor")}
            type="checkbox"
          />

          {doorCheckbox && (
            <Input
              register={register}
              name="doorQuantity"
              label="Quantas portas?"
              placeholder=""
              type="number"
            />
          )}
        </div>

        <S.ButtonWrapper>
          {walls.length < 3 && (
            <Button disabled={!isValid} type="submit">
              Próxima parede
            </Button>
          )}

          {walls.length === 3 && (
            <Button disabled={!isValid} onClick={handleNextStep}>
              Próxima etapa!
            </Button>
          )}
        </S.ButtonWrapper>
      </S.Form>
    </Card>
  );
};

export default RegisterWall;
