import { PaitingProvider } from "contexts/PaitingContext";
import React from "react";
import ReactDOM from "react-dom/client";
import { ThemeProvider } from "styled-components";

import Router from "./routes";
import GlobalStyles from "./styles/global";
import theme from "./styles/theme";

ReactDOM.createRoot(document.getElementById("root") as HTMLElement).render(
  <React.StrictMode>
    <PaitingProvider>
      <ThemeProvider theme={theme}>
        <Router />
        <GlobalStyles />
      </ThemeProvider>
    </PaitingProvider>
  </React.StrictMode>,
);
