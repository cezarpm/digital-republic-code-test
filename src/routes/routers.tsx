import PaintJourney from "views/PaintJourney";

import Home from "../views/Home/Home";

export const routes = [
  {
    path: "/",
    element: <Home />,
  },
  {
    path: "/paint-journey",
    element: <PaintJourney />,
  },
];
