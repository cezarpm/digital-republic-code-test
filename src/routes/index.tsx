import { BrowserRouter, PathRouteProps, Route, Routes } from "react-router-dom";

import { routes } from "./routers";

const Router = () => {
  return (
    <BrowserRouter>
      <Routes>
        {routes.map((props: PathRouteProps) => (
          <Route key={props.path} path={props.path} element={props.element} />
        ))}
      </Routes>
    </BrowserRouter>
  );
};

export default Router;
