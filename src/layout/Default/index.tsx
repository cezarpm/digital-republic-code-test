import * as S from "./styles";

type DefaultProps = {
  children: React.ReactNode;
};

const Default = ({ children }: DefaultProps) => {
  return <S.Wrapper>{children}</S.Wrapper>;
};

export default Default;
